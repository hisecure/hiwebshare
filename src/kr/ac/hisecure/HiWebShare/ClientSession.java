/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kr.ac.hisecure.HiWebShare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author Sanghyeok
 */
public class ClientSession extends Thread {

    private static final Pattern REQUEST_REGEX = Pattern.compile("^([A-Z]+) ([/].*) HTTP\\/(.*)$", Pattern.CASE_INSENSITIVE);

    private final Socket clientSocket;
    private final String remoteAddress;

    public ClientSession(Socket clientSocket) {
        this.clientSocket = clientSocket;
        this.remoteAddress = clientSocket.getRemoteSocketAddress().toString();

        Logger.printInfo("Connected, " + this.remoteAddress);
    }

    @Override
    public void run() {
        try {
            try (InputStreamReader isr = new InputStreamReader(clientSocket.getInputStream())) {
                try (BufferedReader br = new BufferedReader(isr)) {
                    String method = null;
                    String path = null;
                    String version = null;

                    while (true) {
                        String line = br.readLine();
                        Matcher matcher;

                        if (line == null) {
                            break;
                        }

                        matcher = REQUEST_REGEX.matcher(line);
                        if (matcher.matches()) {
                            method = matcher.group(1);
                            path = matcher.group(2);
                            version = matcher.group(3);

                            break;
                        }

                        if (line.isEmpty()) {
                            break;
                        }
                    }

                    HttpResponse response = processRequest(method, path, version);
                    Logger.printInfo(remoteAddress + ", " + method + " " + path);

                    try (OutputStream os = clientSocket.getOutputStream()) {
                        os.write(response.getHeader().getBytes("UTF-8"));
                        os.write("\r\n".getBytes("UTF-8"));

                        switch (response.getType()) {
                            case HttpResponseType.TEXT:
                                os.write(response.getContent().getBytes("UTF-8"));
                                break;
                            case HttpResponseType.BINARY:
                                try (FileInputStream reader = new FileInputStream(response.getFile())) {
                                    byte[] buffer = new byte[1024 * 1024];
                                    int read;

                                    while ((read = reader.read(buffer)) > 0) {
                                        os.write(buffer, 0, read);
                                    }
                                }

                                break;
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.printWarning(ex.getClass().getName() + ", " + ex.getMessage());
        } finally {
            try {
                Logger.printInfo("Disconnected, " + remoteAddress);

                clientSocket.shutdownOutput();
                clientSocket.close();
            } catch (IOException ex) {

            }
        }
    }

    private HttpResponse processRequest(String method, String path, String version) throws UnsupportedEncodingException, IOException {
        HttpResponse response;

        if (!method.equals("GET")) {
            response = new HttpResponse(405);
            response.setType(HttpResponseType.TEXT);
            response.appendContent("Method not allowed.");
            response.appendHeader("Content-Type", "text/html;charset=UTF-8");
        } else {
            File root;
            File target;
            path = URLDecoder.decode(path, "UTF-8");

            if (path.startsWith("/webui/")) {
                root = new File("webui");
                target = new File("webui" + path.substring("/webui/".length() - 1));
            } else {
                root = new File("docs");
                target = new File("docs" + path);
            }

            String rootPath = root.getCanonicalPath();
            String targetPath = target.getCanonicalPath();

            if (targetPath.startsWith(rootPath)) {
                if (target.exists()) {
                    response = new HttpResponse(200);

                    if (target.isDirectory()) {
                        response.setType(HttpResponseType.TEXT);
                        response.appendHeader("Content-Type", "text/html;charset=UTF-8");

                        response.appendContent(ResourceManager.getResource("/res/header.txt").replaceAll("#PATH#", path));
                        response.appendContent(path);
                        response.appendContent(ResourceManager.getResource("/res/body.txt"));

                        File[] files = target.listFiles();

                        if (files.length > 0) {
                            for (File file : files) {
                                if (!file.isDirectory()) {
                                    continue;
                                }

                                String item = ResourceManager.getResource("/res/item.txt");
                                String desc = getMimeType(file);
                                String name1 = file.getName() + "/";

                                item = item.replaceAll("#NAME1#", name1);
                                item = item.replaceAll("#NAME2#", file.getName());
                                item = item.replaceAll("#SIZE#", "-");
                                item = item.replaceAll("#DESC#", desc);

                                response.appendContent(item);
                            }

                            for (File file : files) {
                                if (file.isDirectory()) {
                                    continue;
                                }

                                String item = ResourceManager.getResource("/res/item.txt");
                                String desc = getMimeType(file);
                                String name1 = file.getName();

                                item = item.replaceAll("#NAME1#", name1);
                                item = item.replaceAll("#NAME2#", file.getName());
                                item = item.replaceAll("#SIZE#", String.valueOf(file.length()));
                                item = item.replaceAll("#DESC#", desc);

                                response.appendContent(item);
                            }
                        } else {
                            response.appendContent(ResourceManager.getResource("/res/noitem.txt"));
                        }

                        response.appendContent(ResourceManager.getResource("/res/footer.txt"));
                    } else {
                        response.setType(HttpResponseType.BINARY);
                        response.appendHeader("Content-Type", getMimeType(target));
                        response.appendHeader("Content-Length", String.valueOf(target.length()));
                        response.appendHeader("Content-Disposition", "attachment;filename*=UTF-8\'\'" + URLEncoder.encode(target.getName(), "UTF-8"));
                        response.setFile(target);
                    }
                } else {
                    response = new HttpResponse(404);
                    response.setType(HttpResponseType.TEXT);
                    response.appendContent("Not found.");
                    response.appendHeader("Content-Type", "text/plain;charset=UTF-8");
                }
            } else {
                response = new HttpResponse(403);
                response.setType(HttpResponseType.TEXT);
                response.appendContent("Forbidden.");
                response.appendHeader("Content-Type", "text/plain;charset=UTF-8");
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", java.util.Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        response.appendHeader("Date", sdf.format(new Date()) + " GMT");
        response.appendHeader("Server", "HiWebShare");
        response.appendHeader("Connection", "close");

        return response;
    }

    private String getMimeType(File file) throws IOException {
        String desc = Files.probeContentType(Paths.get(file.getPath()));

        if (file.isDirectory()) {
            desc = "Directory";
        }

        if (desc == null) {
            desc = "application/octet-stream";
        }

        return desc;
    }
}
