/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kr.ac.hisecure.HiWebShare;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Sanghyeok
 */
public class ResourceManager {

    public static String getResource(String path) throws IOException {
        StringBuilder sb = new StringBuilder();

        try (InputStreamReader isr = new InputStreamReader(System.class.getResourceAsStream(path))) {
            try (BufferedReader br = new BufferedReader(isr)) {
                while (true) {
                    String read = br.readLine();

                    if (read == null) {
                        return sb.toString();
                    }

                    sb.append(read);
                }
            }
        }
    }
}
