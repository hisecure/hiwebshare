/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kr.ac.hisecure.HiWebShare;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;

/**
 *
 * @author Sanghyeok
 */
public class MainClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            int port = 8080;
            
            if (args.length > 0) {
                port = Integer.parseInt(args[0]);
            }
            
            File docs = new File("docs");
            docs.mkdirs();
            
            try (ServerSocket listener = new ServerSocket(port)) {
                Logger.printInfo("HiWebShare started, listening port " + port);
                
                for (;;) {
                    (new ClientSession(listener.accept())).start();
                }
            }
        } catch (NumberFormatException | IOException ex) {
            Logger.printFatal(ex.getClass().getName() + ", " + ex.getMessage());
        }
    }
}
