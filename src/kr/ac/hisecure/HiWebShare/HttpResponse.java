/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kr.ac.hisecure.HiWebShare;

import java.io.File;

/**
 *
 * @author Sanghyeok
 */
public class HttpResponse {

    private final StringBuilder header;
    private final StringBuilder content;
    private File file;
    private int type;

    public HttpResponse(int statusCode) {
        header = new StringBuilder();
        content = new StringBuilder();
        file = null;
        
        header.append(getHTTPStatusHeader(statusCode));
    }
    
    public void appendHeader(String name, String value) {
        header.append(name);
        header.append(": ");
        header.append(value);
        header.append("\r\n");
    }
    
    public void appendContent(String content) {
        this.content.append(content);
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    
    public void setType(int type) {
        this.type = type;
    }
    
    public int getType() {
        return type;
    }
    
    public String getHeader() {
        return header.toString();
    }
    
    public String getContent() {
        return content.toString();
    }

    private String getHTTPStatusHeader(int statusCode) {
        switch (statusCode) {
            case 200:
                return "HTTP/1.1 200 OK\r\n";
            case 301:
                return "HTTP/1.1 301 Moved Permanently\r\n";
            case 400:
                return "HTTP/1.1 400 Bad Request\r\n";
            case 403:
                return "HTTP/1.1 403 Permission Denied\r\n";
            case 404:
                return "HTTP/1.1 404 Not Found\r\n";
            case 405:
                return "HTTP/1.1 405 Method Not Allowed\r\n";
            case 418:
                return "HTTP/1.1 418 I'm a teapot\r\n";
            case 501:
                return "HTTP/1.1 501 Not Implemented\r\n";
            case 502:
                return "HTTP/1.1 502 Bad Gateway\r\n";
            default:
                return "HTTP/1.1 500 Internal Server Error\r\n";
        }
    }
}
