/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kr.ac.hisecure.HiWebShare;

/**
 *
 * @author Sanghyeok
 */
public class Logger {
    
    private static void print(String level, String msg) {
        System.out.println(level + ": " + msg);
    }
    
    public static void printInfo(String msg) {
        print("INFO", msg);
    }
    
    public static void printWarning(String msg) {
        print("WARNING", msg);
    }
    
    public static void printFatal(String msg) {
        print("FATAL", msg);
    }
}
